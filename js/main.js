let objectt = {
    name: "john",
    number: 10,
    table: [7, 15, 14]
}
function copy(object) {
    let objectCopy = {}
    for (let key in object) {
        if (Array.isArray(object[key])) {
            objectCopy[key] = copy(object[key])
        } else {
            objectCopy[key] = object[key]
        }
    }
    return (objectCopy)
}
let my = copy(objectt)
console.log(my);
